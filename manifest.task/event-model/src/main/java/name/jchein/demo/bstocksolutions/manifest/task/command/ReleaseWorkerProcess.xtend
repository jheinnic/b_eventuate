package name.jchein.demo.bstocksolutions.manifest.task.command

import de.oehme.xtend.contrib.Buildable
import org.eclipse.xtend.lib.annotations.Data
import javax.validation.constraints.NotNull
import java.util.UUID

@Data
@Buildable
class ReleaseWorkerProcess implements ManifestImportCommand {
	@NotNull
	UUID workerId
}
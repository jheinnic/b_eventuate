package name.jchein.demo.bstocksolutions.manifest.task.command;

import de.oehme.xtend.contrib.Buildable
import org.eclipse.xtend.lib.annotations.Data;
import org.hibernate.validator.constraints.Email
import name.jchein.common.validation.constraints.UUIDString

@Data
@Buildable
public class BeginManifestImportActivity implements ManifestImportCommand {
	@UUIDString
	private String marketplaceId;

	@Email
	private String email;

	@UUIDString
	private String locationId;
}

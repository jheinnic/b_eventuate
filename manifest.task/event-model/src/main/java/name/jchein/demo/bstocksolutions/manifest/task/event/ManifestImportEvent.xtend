package name.jchein.demo.bstocksolutions.manifest.task.event

import io.eventuate.Event
import io.eventuate.EventEntity

@EventEntity(entity="name.jchein.demo.bstocksolutions.manifest.task.aggregate.ManifestImportActivity")
interface ManifestImportEvent extends Event {
}

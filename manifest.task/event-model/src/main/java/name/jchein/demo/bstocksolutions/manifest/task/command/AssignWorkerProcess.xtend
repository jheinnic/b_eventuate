package name.jchein.demo.bstocksolutions.manifest.task.command

import java.util.UUID
import javax.validation.constraints.NotNull
import de.oehme.xtend.contrib.Buildable
import org.eclipse.xtend.lib.annotations.Data
import javax.validation.Valid
import name.jchein.demo.bstocksolutions.manifest.task.value.WorkerLocation

@Data
@Buildable
class AssignWorkerProcess implements ManifestImportCommand {
	@NotNull
	UUID workerId
	
	@NotNull
	@Valid
	WorkerLocation locator
}
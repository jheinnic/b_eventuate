package name.jchein.demo.bstocksolutions.manifest.task.event
import de.oehme.xtend.contrib.Buildable
import org.eclipse.xtend.lib.annotations.Data;
import name.jchein.common.validation.constraints.UUIDString
import org.hibernate.validator.constraints.Email

@Data
@Buildable
class BeganManifestImportActivity implements ManifestImportEvent {
	@UUIDString
	private String marketplace_id;

	@Email
	private String email;

	@UUIDString
	private String location_id;
	
}
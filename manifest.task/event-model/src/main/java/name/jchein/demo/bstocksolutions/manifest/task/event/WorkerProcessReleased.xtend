package name.jchein.demo.bstocksolutions.manifest.task.event

import de.oehme.xtend.contrib.Buildable
import org.eclipse.xtend.lib.annotations.Data
import javax.validation.constraints.NotNull
import java.util.UUID

@Data
@Buildable
class WorkerProcessReleased implements ManifestImportEvent {
	@NotNull
	UUID workerId
}
package name.jchein.demo.bstocksolutions.manifest.task.service

import name.jchein.demo.bstocksolutions.manifest.task.aggregate.ManifestImportActivity
import name.jchein.demo.bstocksolutions.manifest.task.command.ManifestImportCommand
import name.jchein.demo.bstocksolutions.manifest.task.command.AssignWorkerProcess
import io.eventuate.AggregateRepository
import java.util.concurrent.CompletableFuture
import io.eventuate.EntityWithIdAndVersion
import org.hibernate.validator.constraints.Email
import name.jchein.demo.bstocksolutions.manifest.task.command.BeginManifestImportActivity
import javax.validation.constraints.NotNull
import java.util.UUID
import javax.validation.executable.ValidateOnExecution
import javax.validation.executable.ExecutableType
import org.springframework.stereotype.Component
import org.springframework.beans.factory.annotation.Autowired
import name.jchein.common.validation.constraints.UUIDString
import io.eventuate.SaveOptions
import static extension java.util.Optional.of
import name.jchein.common.validation.constraints.ResolvesToIP
import javax.validation.constraints.Pattern
import org.hibernate.validator.constraints.Range
import name.jchein.demo.bstocksolutions.manifest.task.value.WorkerLocation

@Component
@ValidateOnExecution(type=ExecutableType.ALL)
class ManifestActivityService {
	private final AggregateRepository<ManifestImportActivity, ManifestImportCommand> taskRepository;

	@Autowired
	new(AggregateRepository<ManifestImportActivity, ManifestImportCommand> taskRepository) {
		this.taskRepository = taskRepository;
	}

	private def toSaveOptions(String activityId) {
		return new SaveOptions().withId(activityId).of()
	}
	
	def CompletableFuture<EntityWithIdAndVersion<ManifestImportActivity>> beginImportActivity(
		@NotNull @UUIDString String activityId,
		@Email @NotNull String email,
		@NotNull @UUIDString String locationId,
		@NotNull @UUIDString String marketplaceId
	) {
		return this.taskRepository.save(
			BeginManifestImportActivity.build [
				it.email(email)
				.locationId(locationId)
				.marketplaceId(marketplaceId)
			],
			activityId.toSaveOptions()
		)
	}

	def CompletableFuture<EntityWithIdAndVersion<ManifestImportActivity>> assignWorkerProcess(
		@NotNull @UUIDString String activityId,
		@NotNull UUID workerId,
		@NotNull @ResolvesToIP String nodeAddress,
		@Range(min=2, max=65536) int processId,
		@NotNull @Pattern(regexp="/([a-zA-Z0-9]*\\/)*/") String fileCachePath
	) {
		return this.taskRepository.update(
			activityId,
			AssignWorkerProcess.build[
				it.workerId(workerId)
				.locator(
					WorkerLocation.build[
						it.nodeAddress(nodeAddress)
						.processId(processId)
						.fileCachePath(fileCachePath)
					]
				)
			]
		)	
	}
}

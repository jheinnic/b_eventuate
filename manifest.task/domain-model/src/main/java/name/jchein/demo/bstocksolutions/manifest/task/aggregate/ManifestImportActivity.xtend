package name.jchein.demo.bstocksolutions.manifest.task.aggregate

import io.eventuate.Event
import io.eventuate.ReflectiveMutableCommandProcessingAggregate
import java.util.ArrayList
import java.util.List
import name.jchein.demo.bstocksolutions.manifest.task.command.AssignWorkerProcess
import name.jchein.demo.bstocksolutions.manifest.task.command.BeginManifestImportActivity
import name.jchein.demo.bstocksolutions.manifest.task.command.ManifestImportCommand
import name.jchein.demo.bstocksolutions.manifest.task.command.ReleaseWorkerProcess
import name.jchein.demo.bstocksolutions.manifest.task.event.BeganManifestImportActivity
import name.jchein.demo.bstocksolutions.manifest.task.event.WorkerProcessAssigned
import name.jchein.demo.bstocksolutions.manifest.task.event.WorkerProcessReleased

class ManifestImportActivity extends ReflectiveMutableCommandProcessingAggregate<ManifestImportActivity, ManifestImportCommand> {
	def List<Event> asList(Event evt) {
		val retVal = new ArrayList<Event>(1);
		retVal.add(evt);
		return retVal;
	}

	def List<Event> process(BeginManifestImportActivity cmd) {
		return BeganManifestImportActivity.build [
			it.email(cmd.email).location_id(cmd.locationId).marketplace_id(cmd.marketplaceId)
		].asList()
	}

	def List<Event> process(AssignWorkerProcess cmd) {
		return WorkerProcessAssigned.build [
			it.workerId(cmd.workerId).locator(cmd.locator)
		].asList()
	}

	def List<Event> process(ReleaseWorkerProcess cmd) {
		return WorkerProcessReleased.build [
			it.workerId(cmd.workerId)
		].asList()
	}

	def void apply(BeganManifestImportActivity evt) {
	}

	def void apply(WorkerProcessAssigned evt) {
	}

	def void apply(WorkerProcessReleased evt) {
	}
}

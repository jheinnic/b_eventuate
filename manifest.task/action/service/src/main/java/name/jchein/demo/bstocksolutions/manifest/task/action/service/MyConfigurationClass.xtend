package name.jchein.demo.bstocksolutions.manifest.task.action.service

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import io.eventuate.AggregateRepository
import io.eventuate.EventuateAggregateStore
//import io.eventuate.javaclient.spring.EnableEventHandlers;
import name.jchein.demo.bstocksolutions.manifest.task.aggregate.ManifestImportActivity
import name.jchein.demo.bstocksolutions.manifest.task.command.ManifestImportCommand
import name.jchein.demo.bstocksolutions.manifest.task.service.ManifestActivityService

@Configuration
//@PropertySource(encoding = "UTF-8", value = "classpath:application.properties")
//@Import(#[,WebConfiguration])
@ComponentScan(#["name.jchein.demo.bstocksolutions.manifest.task.action.service",
	"name.jchein.demo.bstocksolutions.manifest.task.action.service.spring"])
class MyConfigurationClass {
	@Bean
	def ManifestActivityService customerService(
		AggregateRepository<ManifestImportActivity, ManifestImportCommand> manifestTaskRepository
	) {
		return new ManifestActivityService(manifestTaskRepository)
	}

	@Bean
	def AggregateRepository<ManifestImportActivity, ManifestImportCommand> customerRepository(
		EventuateAggregateStore eventStore
	) {
		return new AggregateRepository<ManifestImportActivity, ManifestImportCommand>(ManifestImportActivity, eventStore)
	}
}

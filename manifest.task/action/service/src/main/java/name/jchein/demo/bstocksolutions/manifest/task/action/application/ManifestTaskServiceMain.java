package name.jchein.demo.bstocksolutions.manifest.task.action.application;

import io.eventuate.local.java.jdbckafkastore.EventuateLocalConfiguration;

import name.jchein.demo.bstocksolutions.common.web.ServletContainerConfiguration;
import name.jchein.demo.bstocksolutions.common.web.WebConfiguration;
import name.jchein.demo.bstocksolutions.common.websocket.WebSocketConfig;
import name.jchein.demo.bstocksolutions.common.websocket.WebSocketSecurityConfig;
import name.jchein.demo.bstocksolutions.manifest.task.action.service.spring.SwaggerDocumentationConfig;
import name.jchein.demo.bstocksolutions.manifest.task.action.service.MyConfigurationClass;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableAutoConfiguration
@ComponentScan
@Import({
	WebConfiguration.class, 
	ServletContainerConfiguration.class, 
	EventuateLocalConfiguration.class,
	WebSocketConfig.class, 
	WebSocketSecurityConfig.class,
	MyConfigurationClass.class,
	SwaggerDocumentationConfig.class 
})
public class ManifestTaskServiceMain {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(ManifestTaskServiceMain.class);
		app.setWebEnvironment(true);
		app.setHeadless(true);
		app.setBannerMode(Banner.Mode.OFF);
		app.setLogStartupInfo(true);
		app.run(args);
	}
}

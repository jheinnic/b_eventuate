package name.jchein.demo.bstocksolutions.manifest.task.action.service.impl

import javax.validation.constraints.NotNull
import javax.validation.executable.ExecutableType
import javax.validation.executable.ValidateOnExecution
import name.jchein.demo.bstocksolutions.common.web.util.BadRequestException
import name.jchein.demo.bstocksolutions.manifest.task.action.service.api.UserFacingActionsApiDelegate
import name.jchein.demo.bstocksolutions.manifest.task.action.service.model.CreateManifestIngestTask
import name.jchein.demo.bstocksolutions.manifest.task.action.service.model.EmptyMessage
import name.jchein.demo.bstocksolutions.manifest.task.service.ManifestActivityService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.util.concurrent.CompletableToListenableFutureAdapter

@Component
@ValidateOnExecution(type=ExecutableType.ALL)
class UserFacingActionsApi implements UserFacingActionsApiDelegate {
	val ManifestActivityService manifestTaskService

	new(@NotNull ManifestActivityService manifestTaskService) {
		this.manifestTaskService = manifestTaskService
	}

	def override createManifestIngestTask(String id, CreateManifestIngestTask body) {
		val retVal = this.manifestTaskService.beginImportActivity(
			id,
			body.manifestOrigin.email,
			body.manifestOrigin.locationId,
			body.manifestOrigin.marketplaceId
		)

		val retVal2 = retVal.thenApply [
			if (it.entityId != id) {
				throw new BadRequestException("Wrong Entity ID")
			}
			return new ResponseEntity<Void>(null as Void, HttpStatus.CREATED)
		]
		
		return new CompletableToListenableFutureAdapter<ResponseEntity<Void>>(retVal2)
	}
}

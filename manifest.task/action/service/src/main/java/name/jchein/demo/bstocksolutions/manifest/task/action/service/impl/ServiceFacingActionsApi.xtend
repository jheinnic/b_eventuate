package name.jchein.demo.bstocksolutions.manifest.task.action.service.impl

import name.jchein.demo.bstocksolutions.manifest.task.action.service.api.ServiceFacingActionsApiDelegate
import org.springframework.stereotype.Component
import name.jchein.demo.bstocksolutions.manifest.task.service.ManifestActivityService
import javax.validation.constraints.NotNull
import javax.validation.executable.ValidateOnExecution
import javax.validation.executable.ExecutableType
import org.springframework.util.concurrent.CompletableToListenableFutureAdapter
import name.jchein.demo.bstocksolutions.manifest.task.action.service.model.AssignWorkerProcess
import name.jchein.demo.bstocksolutions.manifest.task.action.service.model.ReleaseWorkerProcess
import java.util.UUID

@Component
@ValidateOnExecution(type=ExecutableType.ALL)
class ServiceFacingActionsApi implements ServiceFacingActionsApiDelegate {
	val ManifestActivityService manifestTaskService

	new(@NotNull ManifestActivityService manifestTaskService) {
		this.manifestTaskService = manifestTaskService
	}


	override ingestManifestTasksIdAssignWorkerPut(String id, AssignWorkerProcess body) {
		val retVal = this.manifestTaskService.assignWorkerProcess(
			id,
			UUID.fromString(body.workerId),
			body.locator.nodeAddress,
			body.locator.processId,
			body.locator.fileCachePath
		)

		return new CompletableToListenableFutureAdapter(retVal)

	}

	override ingestManifestTasksIdReleaseWorkerPut(String id, ReleaseWorkerProcess body) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

}

CREATE DATABASE bstock;
GRANT ALL PRIVILEGES ON bstock.* TO 'bstock'@'%' WITH GRANT OPTION;
USE bstock;
DROP TABLE IF EXISTS events;
DROP TABLE IF EXISTS entities;
DROP TABLE IF EXISTS snapshots;

CREATE TABLE events (
  event_id varchar(1000) PRIMARY KEY,
  event_type varchar(1000),
  event_data varchar(1000) NOT NULL,
  entity_type VARCHAR(1000) NOT NULL,
  entity_id VARCHAR(1000) NOT NULL,
  triggering_event VARCHAR(1000),
  metadata VARCHAR(1000)
) ENGINE = INNODB CHARACTER SET ascii;

CREATE INDEX events_idx ON events(entity_type, entity_id, event_id);

CREATE TABLE entities (
  entity_type VARCHAR(1000),
  entity_id VARCHAR(1000),
  entity_version VARCHAR(1000) NOT NULL,
  PRIMARY KEY(entity_type, entity_id)
) ENGINE = INNODB CHARACTER SET ascii; 

CREATE INDEX entities_idx ON events(entity_type, entity_id);

CREATE TABLE snapshots (
  entity_type VARCHAR(1000),
  entity_id VARCHAR(1000),
  entity_version VARCHAR(1000),
  snapshot_type VARCHAR(1000) NOT NULL,
  snapshot_json VARCHAR(1000) NOT NULL,
  triggering_events VARCHAR(1000),
  PRIMARY KEY(entity_type, entity_id, entity_version)
) ENGINE = INNODB CHARACTER SET ascii;

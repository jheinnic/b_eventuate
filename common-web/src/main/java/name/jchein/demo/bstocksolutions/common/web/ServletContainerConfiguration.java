package name.jchein.demo.bstocksolutions.common.web;

import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

import name.jchein.demo.bstocksolutions.common.web.filter.CORSFilter;
import name.jchein.demo.bstocksolutions.common.web.util.HttpExceptionHandler;

@Configuration
public class ServletContainerConfiguration {

	@Bean
	public EmbeddedServletContainerFactory servletContainer() {
		TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
//		factory.setPort(9000);
//		factory.setSessionTimeout(10, TimeUnit.MINUTES);
		factory.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/notfound.html"));
		return factory;
	}


    @Bean
    public CORSFilter corsFilter() {
    		return new CORSFilter();
    }
    
    @Bean
    public HttpExceptionHandler httpExceptionHandler() {
    		return new HttpExceptionHandler();
    }
}

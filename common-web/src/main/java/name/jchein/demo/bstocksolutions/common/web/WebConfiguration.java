package name.jchein.demo.bstocksolutions.common.web;

import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
public class WebConfiguration extends WebMvcConfigurerAdapter {
//    @Bean
//    public ServletListenerRegistrationBean<RequestContextListener> httpRequestContextListener() {
//        return new ServletListenerRegistrationBean<RequestContextListener>(new RequestContextListener());
//    }
 
    @Override
	public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
		HttpMessageConverter<?> additional = new MappingJackson2HttpMessageConverter();
		converters.add(additional);
	}
}
